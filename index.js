//[SECTION] JavaScript Syncgronous vs Asynchronous

// JavaScript is by default synchronous meaning that it only executes on statement a a time


console.log("Hello World");
//conosle.log("Hello Again");
console.log("Good Bye");

//bode blocking - waiting for the specific statement to finish before executing the next statement
// for (let i = 0; i <= 1500; i++){
//   console.log(i);
// }
console.log("Hello Again");

// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background.

// [SECTION] Getting all posts

// The Fetch API that  allows us to asynchronously request for a resource (data).

// "fetch()" method in JavaScript is used to request to the server and load information on the webpages.

//Syntax
// fetch("apiURL")

// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value.

//A promise may be in one of 3 possible states: fuillfilled, rejected or pending.

/*
  pending: initial states, neither fullfiled nor rejected response.
    fullfiled: operation was completed
    rejected: operation failed:
    pending:

    syntax:
    fetch("apiURL").then((response)=> {})

*/

//the ".then()" method captures the response object and returns another promise which will be either "rsolved" or "rejected".
fetch("https://jsonplaceholder.typicode.com/posts")
  // .then(response => console.log(response.status));

  //"json()" method will convert JSON format to JavaScript object
  .then(response => response.json())
  .then(json => console.log(json));
// .then(json => {
//   json.forEach(post => console.log(post.title))
// });

// The "async" and "await" keyword to achieve asynchronous code.

async function fetchData() {
  let result = await (fetch("https://jsonplaceholder.typicode.com/posts"));
  //Returned the "response" Object
  console.log(result);
  let json = await result.json()
  console.log(json);
}

fetchData()


//[SECTION]  getting a specific posts
//retrieves a specific post following the REST api (retrieve, /posts/ :id, GET)
// "id:" is a wildcardw here you can put any value, it then creates a link between "id" pearameter in the URL and the  value provided in the URL

fetch("https://jsonplaceholder.typicode.com/posts/1")
  .then(response => response.json()).then(json => console.log(json));


// [SECTION] Creating a posts

/*
  Syntax:
    fetch("apiURL", options)
    .then((response) => {})
    .then((response) => {})

*/

fetch("https://jsonplaceholder.typicode.com/posts", {
  method: "POST",
  headers: {
    "Content-Type": "application/json"
  },
  body:JSON.stringify({
    title:"New Post",
    body: "Hello World!",
    userId: 1
  })
})
.then(response => response.json())
.then(json => console.log(json));

//[SECTION] updating a posts

fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "PUT",
  headers: {
    "Content-Type": "application/json"
  },
  body:JSON.stringify({
    title:"updated post",
    body: "Hello Again",
    userId: 1
  })
})
.then(response => response.json())
.then(json => console.log(json));


// [SECTION] UPDATING a post using PATCH method
// PUT vs PATCH
  // patch is used to updated a single/several properties
  //PUT is used to update the whole document

fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "PUT",
  headers: {
    "Content-Type": "application/json"
  },
  body:JSON.stringify({
    title:"Corrected post title",
  })
})
.then(response => response.json())
.then(json => console.log(json));

//[SETION] Delete/archiving of a posts

fetch("https://jsonplaceholder.typicode.com/posts/1", {method:"DELETE"})

.then(response => response.json())
.then(json => console.log(json));








//
